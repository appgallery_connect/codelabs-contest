package com.rick.doubleblind.fr

import android.view.View
import com.blankj.utilcode.util.KeyboardUtils
import com.blankj.utilcode.util.ToastUtils
import com.huawei.agconnect.AGConnectInstance
import com.huawei.agconnect.auth.AGConnectAuth
import com.huawei.agconnect.cloud.database.*
import com.huawei.hmf.tasks.OnFailureListener
import com.huawei.hmf.tasks.OnSuccessListener
import com.rick.doubleblind.bean.User
import com.rick.doubleblind.clouddb.Friend
import com.rick.doubleblind.clouddb.ObjectTypeInfoHelper
import com.rick.doubleblind.databinding.FrMainBinding
import com.rick.doubleblind.databinding.ItemFriendBinding
import com.rick.doubleblind.vm.MainVm
import com.rick.jetpackmvvm.base.BaseFragment
import com.rick.jetpackmvvm.base.BaseListAdapter2
import java.util.*

open class MainFr : BaseFragment<FrMainBinding, MainVm>() {
    private var cloudDB: AGConnectCloudDB? = null
    private var cloudDBZone: CloudDBZone? = null

    override fun init(binding: FrMainBinding, vm: MainVm) {
        binding.vm = vm
        binding.fr = this
        binding.adapter = Adapter()
    }

    override fun load(): Boolean {
        login()
        return true
    }

    fun login() {
        signInAnonymously({
            viewModel.user.value = it
            KeyboardUtils.hideSoftInput(requireActivity())
        }, { ToastUtils.showShort(it.message) })
    }

    /**
     * 匿名登录
     * @param onSuccessListener 成功回调
     * @param onFailureListener 失败回调
     */
    private fun signInAnonymously(
        onSuccessListener: OnSuccessListener<User>,
        onFailureListener: OnFailureListener,
    ) {
        AGConnectAuth.getInstance().currentUser.let { agConnectUser -> // 查询登录用户信息
            if (agConnectUser == null) { // 没有登录用户信息
                AGConnectAuth.getInstance().signInAnonymously() // 匿名登录
                    .addOnSuccessListener { // 匿名登录成功
                        openCloudDBZone(it.user.uid, onSuccessListener, onFailureListener) // 打开云数据库
                    }
                    .addOnFailureListener(onFailureListener) // 匿名登录失败
            } else { // 有登录用户信息
                openCloudDBZone(agConnectUser.uid, onSuccessListener, onFailureListener) // 打开云数据库
            }
        }
    }

    /**
     * 打开云数据库
     * @param uid 用户ID
     * @param onSuccessListener 成功回调
     * @param onFailureListener 失败回调
     */
    private fun openCloudDBZone(
        uid: String,
        onSuccessListener: OnSuccessListener<User>,
        onFailureListener: OnFailureListener,
    ) {
        if (cloudDBZone == null) { // 云数据库未打开
            if (cloudDB == null) {
                cloudDB = AGConnectInstance.getInstance()
                    .let { AGConnectCloudDB.getInstance(it, AGConnectAuth.getInstance(it)) }
            }
            cloudDB!!.let { cloudDB ->
                cloudDB.createObjectType(ObjectTypeInfoHelper.getObjectTypeInfo())
                // 打开云数据库
                cloudDB.openCloudDBZone2(CloudDBZoneConfig("doubleblind",
                    CloudDBZoneConfig.CloudDBZoneSyncProperty.CLOUDDBZONE_CLOUD_CACHE,
                    CloudDBZoneConfig.CloudDBZoneAccessProperty.CLOUDDBZONE_PUBLIC).apply {
                    persistenceEnabled = true
                }, true)
                    .addOnSuccessListener { // 打开云数据库成功
                        cloudDBZone = it
                        queryUser(uid, onSuccessListener, onFailureListener) // 查询用户信息
                    }.addOnFailureListener(onFailureListener) // 打开云数据库失败
            }
        } else { // 云数据库已打开
            queryUser(uid, onSuccessListener, onFailureListener) // 查询用户信息
        }
    }

    /**
     * 查询用户信息
     * @param uid 用户ID
     * @param onSuccessListener 成功回调
     * @param onFailureListener 失败回调
     */
    private fun queryUser(
        uid: String,
        onSuccessListener: OnSuccessListener<User>,
        onFailureListener: OnFailureListener,
    ) {
        cloudDBZone?.let { cloudDBZone ->
            // 查询用户信息
            query(CloudDBZoneQuery.where(com.rick.doubleblind.clouddb.User::class.java)
                .equalTo("uid", uid), { // 查询用户信息成功
                if (it.isNotEmpty()) { // 有用户信息
                    val user = it[0]
                    queryFriends(user,
                        { onSuccessListener.onSuccess(User(user, it)) },
                        onFailureListener) // 查询好友信息
                } else { // 没有用户信息
                    viewModel.name.value.let { name ->
                        if (name.isNotBlank()) {
                            // 新建用户信息
                            val user = com.rick.doubleblind.clouddb.User()
                            user.uid = uid
                            user.name = name
                            // 写入用户信息
                            cloudDBZone.executeUpsert(user)
                                .addOnSuccessListener {
                                    onSuccessListener.onSuccess(User(user,
                                        emptyList()))
                                } // 写入用户信息成功
                                .addOnFailureListener(onFailureListener) // 写入用户信息失败
                        }
                    }
                }
            }, onFailureListener) // 查询用户信息失败
        }
    }

    /**
     * 查询好友信息
     * @param user 用户信息
     * @param onSuccessListener 成功回调
     * @param onFailureListener 失败回调
     */
    private fun queryFriends(
        user: com.rick.doubleblind.clouddb.User,
        onSuccessListener: OnSuccessListener<List<com.rick.doubleblind.clouddb.User>>,
        onFailureListener: OnFailureListener,
    ) {
        // 查询好友uid
        query(CloudDBZoneQuery.where(Friend::class.java).equalTo("uid", user.uid), {
            // 查询好友uid成功
            if (it.isEmpty()) { // 没有好友
                onSuccessListener.onSuccess(emptyList())
            } else { // 有好友
                val uids = it.map { it.friend_uid }.toTypedArray()
                val query = CloudDBZoneQuery.where(com.rick.doubleblind.clouddb.User::class.java)
                    .`in`("uid", uids)
                query(query, onSuccessListener, onFailureListener) // 根据 uid 查询好友用户信息
            }
        }, onFailureListener) // 查询好友uid失败
    }

    /**
     * 随机添加好友
     */
    open fun randomAddFriend() {
        val onFailureListener = OnFailureListener { ToastUtils.showShort(it.message) }
        val user = viewModel.user.value!!
        // 查询不包含自己的用户列表
        query(CloudDBZoneQuery.where(com.rick.doubleblind.clouddb.User::class.java)
            .notEqualTo("uid", user.user.uid), {
            // 查询不包含自己的用户列表成功
            val friendsUid = user.friends.map { it.uid } // 已添加的好友用户
            val enableAddUsers = it.toMutableList() // 可添加的用户
            enableAddUsers.removeAll { friendsUid.contains(it.uid) } // 排除已添加的好友用户
            if (enableAddUsers.isEmpty()) { // 没有可添加的好友用户
                onFailureListener.onFailure(Exception("没有可添加的用户"))
            } else { // 有可添加的好友用户
                // 创建好友关联实例
                val friend = Friend()
                friend.uid = user.user.uid
                val newFriend = enableAddUsers[Random().nextInt(enableAddUsers.size)] // 随机添加一名用户
                friend.friend_uid = newFriend.uid
                // 写入数据库
                cloudDBZone!!.executeUpsert(friend).addOnSuccessListener {
                    // 写入数据库成功
                    ToastUtils.showShort("已添加好友：${newFriend.name}")
                    queryFriends(user.user,
                        { viewModel.user.value = User(user.user, it) },
                        onFailureListener) // 重新查询好友
                }.addOnFailureListener(onFailureListener) // 写入数据库失败
            }
        }, onFailureListener) // 查询不包含自己的用户列表失败
    }

    /**
     * 云数据库查询
     * @param query 查询条件
     * @param onSuccessListener 成功回调
     * @param onFailureListener 失败回调
     */
    private fun <T : CloudDBZoneObject> query(
        query: CloudDBZoneQuery<T>,
        onSuccessListener: OnSuccessListener<List<T>>,
        onFailureListener: OnFailureListener,
    ) {
        cloudDBZone?.let { cloudDBZone ->
            // 执行查询
            cloudDBZone.executeQuery(query,
                CloudDBZoneQuery.CloudDBZoneQueryPolicy.POLICY_QUERY_FROM_CLOUD_ONLY)
                .addOnSuccessListener { // 查询成功
                    // 获取查询结果
                    val result = mutableListOf<T>()
                    while (it.snapshotObjects.hasNext()) {
                        result.add(it.snapshotObjects.next())
                    }
                    onSuccessListener.onSuccess(result) // 回调成功结果
                }.addOnFailureListener(onFailureListener) // 查询失败
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        cloudDB?.closeCloudDBZone(cloudDBZone)
    }

    class Adapter : BaseListAdapter2<com.rick.doubleblind.clouddb.User, ItemFriendBinding>() {
        override fun initItem(
            position: Int,
            bean: com.rick.doubleblind.clouddb.User,
            binding: ItemFriendBinding,
        ) {
            binding.item = bean
            binding.line.visibility = if (position == itemCount - 1) View.GONE else View.VISIBLE
        }
    }
}