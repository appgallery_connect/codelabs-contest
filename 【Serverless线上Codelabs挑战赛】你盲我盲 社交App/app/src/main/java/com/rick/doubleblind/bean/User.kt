package com.rick.doubleblind.bean

import com.rick.doubleblind.clouddb.User

data class User(val user: User, val friends: List<User>)
