package com.rick.doubleblind.vm

import android.graphics.Color
import androidx.lifecycle.MutableLiveData
import com.rick.doubleblind.bean.User
import com.rick.jetpackmvvm.base.BaseVm
import com.rick.jetpackmvvm.commom.NoNullLiveData
import com.rick.jetpackmvvm.commom.NoNullValue

open class AppVm : BaseVm() {
    val user: MutableLiveData<User?> = MutableLiveData()
    val friends = NoNullLiveData(object : NoNullValue<List<com.rick.doubleblind.clouddb.User>> {
        override fun get(): List<com.rick.doubleblind.clouddb.User> {
            return emptyList()
        }
    })

    init {
        statusBarColor.value = Color.WHITE
        user.observeForever {
            if (it == null) {
                friends.value = emptyList()
            } else {
                friends.value = it.friends
            }
        }
    }
}