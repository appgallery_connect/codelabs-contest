package com.rick.doubleblind

import android.app.Application
import android.graphics.Color
import android.view.Gravity
import com.blankj.utilcode.util.ColorUtils
import com.blankj.utilcode.util.ToastUtils
import com.huawei.agconnect.cloud.database.AGConnectCloudDB

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        ToastUtils.getDefaultMaker()
            .setBgColor(ColorUtils.getColor(R.color.purple_500))
            .setTextColor(Color.WHITE)
            .setGravity(Gravity.CENTER, 0, 0)
        AGConnectCloudDB.initialize(this)
    }
}