package com.rick.doubleblind.vm

import com.rick.jetpackmvvm.commom.NoNullLiveData
import com.rick.jetpackmvvm.commom.NoNullValue

class MainVm : AppVm() {
    val name = NoNullLiveData(object : NoNullValue<String> {
        override fun get(): String {
            return ""
        }
    })
}