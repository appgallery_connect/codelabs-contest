package com.example.playtogether;

import com.huawei.agconnect.cloud.database.CloudDBZone;

public class GlobalInfo {
    private static final String TAG = "CloudDB";

    public static CloudDBZone gCloudDBZone;
    public static SportInfo gInfo = new SportInfo();
    public static String gCurrentUser = "";

}
