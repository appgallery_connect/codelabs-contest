package com.example.playtogether;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.huawei.agconnect.cloud.database.CloudDBZoneObjectList;
import com.huawei.agconnect.cloud.database.CloudDBZoneQuery;
import com.huawei.agconnect.cloud.database.CloudDBZoneSnapshot;
import com.huawei.agconnect.cloud.database.exceptions.AGConnectCloudDBException;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;
import com.huawei.hmf.tasks.Task;

import java.text.SimpleDateFormat;
import java.util.Collections;

public class DetailActivity extends AppCompatActivity implements View.OnClickListener{
    private static final String TAG = DetailActivity.class.getSimpleName();

    private ImageView imgBack;

    private TextView edtDate;
    private TextView edtTime;
    private TextView edtName;
    private TextView edtAddr;
    private TextView edtCount;
    private TextView edtRequest;
    private TextView edtUserlist;

    private Button btnRequest;
    private Button btnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.statuscolor));
        }

        imgBack = findViewById(R.id.img_back);
        imgBack.setOnClickListener(this);

        edtName = findViewById(R.id.edt_post_name);
        edtAddr = findViewById(R.id.edt_post_addr);
        edtDate = findViewById(R.id.edt_post_date);
        edtTime = findViewById(R.id.edt_post_time);
        edtCount = findViewById(R.id.edt_post_count);
        edtRequest = findViewById(R.id.edt_post_request);
        edtUserlist = findViewById(R.id.edt_post_userlist);

        btnRequest = findViewById(R.id.btn_request);
        btnRequest.setOnClickListener(this);
        btnCancel = findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);

    }


    @Override
    protected void onStart() {
        super.onStart();

        edtName.setText(GlobalInfo.gInfo.getName());
        edtAddr.setText(GlobalInfo.gInfo.getAddr());
        edtDate.setText(GlobalInfo.gInfo.getTime().substring(0, 10));
        edtTime.setText(GlobalInfo.gInfo.getTime().substring(11));
        edtCount.setText(GlobalInfo.gInfo.getCount().toString());
        edtRequest.setText(GlobalInfo.gInfo.getRequest().toString());

        String[] users = GlobalInfo.gInfo.getUserlist().split(";");
        String list = "";
        for(int i = 0; i < users.length; i++) {
            if(list.length() > 0) {
                list += "\n";
            }
            list = list + users[i].substring(0, 3) + "****" + users[i].substring(7);
        }
        edtUserlist.setText(list);

        if(GlobalInfo.gInfo.getUserlist().contains(GlobalInfo.gCurrentUser)) {
            this.btnRequest.setVisibility(View.GONE);
            this.btnCancel.setVisibility(View.VISIBLE);
        } else {
            this.btnRequest.setVisibility(View.VISIBLE);
            this.btnCancel.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.btn_request:
                checkBeforeRequest();
                break;
            case R.id.btn_cancel:
                if(GlobalInfo.gInfo.getId().equals(GlobalInfo.gCurrentUser)) {
                    Toast.makeText(DetailActivity.this, "自己发布的活动请在发布页面取消！", Toast.LENGTH_SHORT).show();
                } else {
                    cancelRequest();
                }
                break;
        }
    }

    private void cancelRequest() {
        CloudDBZoneQuery<SportInfo> query = CloudDBZoneQuery.where(SportInfo.class).equalTo("id", GlobalInfo.gInfo.getId());

        Task<CloudDBZoneSnapshot<SportInfo>> queryTask = GlobalInfo.gCloudDBZone.executeQuery(query,
                CloudDBZoneQuery.CloudDBZoneQueryPolicy.POLICY_QUERY_FROM_CLOUD_ONLY);
        queryTask.addOnSuccessListener(new OnSuccessListener<CloudDBZoneSnapshot<SportInfo>>() {
            @Override
            public void onSuccess(CloudDBZoneSnapshot<SportInfo> snapshot) {
//                processQueryResult(snapshot);
                CloudDBZoneObjectList<SportInfo> sportInfoCursor = snapshot.getSnapshotObjects();
                try {
                    SportInfo info = sportInfoCursor.get(0);
                    String temp = "";

                    String list = info.getUserlist();
                    if(list.equals(GlobalInfo.gCurrentUser)) {

                    }
                    else if(list.startsWith(GlobalInfo.gCurrentUser)) {
                        info.setRequest(info.getRequest() - 1);
                        temp = list.replace(GlobalInfo.gCurrentUser + ";", "");
                        info.setUserlist(temp);
                        updateSport(info, 2);
                    } else if(list.endsWith(GlobalInfo.gCurrentUser)) {
                        info.setRequest(info.getRequest() - 1);
                        temp = list.replace( ";" + GlobalInfo.gCurrentUser, "");
                        info.setUserlist(temp);
                        updateSport(info, 2);
                    }

                } catch (AGConnectCloudDBException e) {
                    Log.w(TAG, "processQueryResult: " + e.getMessage());
                } finally {
                    snapshot.release();
                }

                Log.i(TAG, "onSuccess: query result success");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Log.i(TAG, "onSuccess: query result failed," + e.toString());
            }
        });
    }

    private void checkBeforeRequest() {
        int count = Integer.parseInt(edtCount.getText().toString());
        int request = Integer.parseInt(edtRequest.getText().toString());
        Log.d(TAG, "id=" + GlobalInfo.gInfo.getId() + ",user=" + GlobalInfo.gCurrentUser);
        if(GlobalInfo.gInfo.getId().equals(GlobalInfo.gCurrentUser)) {
            Toast.makeText(DetailActivity.this, "自己发布的活动不用重复参加！", Toast.LENGTH_SHORT).show();
        } else if(request >= count) {
            Toast.makeText(DetailActivity.this, "名额已满，看看其他活动吧！", Toast.LENGTH_SHORT).show();
        } else if(GlobalInfo.gInfo.getUserlist().contains(GlobalInfo.gCurrentUser)) {
            Toast.makeText(DetailActivity.this, "您已经报过名了！", Toast.LENGTH_SHORT).show();
        } else {
            requestSport();
        }
    }

    private void requestSport() {
//        CloudDBZoneQuery<SportInfo> query = CloudDBZoneQuery.where(SportInfo.class);
        CloudDBZoneQuery<SportInfo> query = CloudDBZoneQuery.where(SportInfo.class).equalTo("id", GlobalInfo.gInfo.getId());

        Task<CloudDBZoneSnapshot<SportInfo>> queryTask = GlobalInfo.gCloudDBZone.executeQuery(query,
                CloudDBZoneQuery.CloudDBZoneQueryPolicy.POLICY_QUERY_FROM_CLOUD_ONLY);
        queryTask.addOnSuccessListener(new OnSuccessListener<CloudDBZoneSnapshot<SportInfo>>() {
            @Override
            public void onSuccess(CloudDBZoneSnapshot<SportInfo> snapshot) {
//                processQueryResult(snapshot);
                CloudDBZoneObjectList<SportInfo> sportInfoCursor = snapshot.getSnapshotObjects();
                try {
                    SportInfo info = sportInfoCursor.get(0);
                    info.setRequest(info.getRequest() + 1);
                    String list = info.getUserlist();
                    info.setUserlist(list + ";" + GlobalInfo.gCurrentUser);

                    updateSport(info, 1);
                    Log.d(TAG, "request=" + info.getRequest());

                } catch (AGConnectCloudDBException e) {
                    Log.w(TAG, "processQueryResult: " + e.getMessage());
                } finally {
                    snapshot.release();
                }

                Log.i(TAG, "onSuccess: query result success");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Log.i(TAG, "onSuccess: query result failed," + e.toString());
            }
        });
    }

    private void updateSport(SportInfo sInfo, int type) {

        Task<Integer> upsertTask = GlobalInfo.gCloudDBZone.executeUpsert(sInfo);
        upsertTask.addOnSuccessListener(new OnSuccessListener<Integer>() {
            @Override
            public void onSuccess(Integer cloudDBZoneResult) {
                Log.i(TAG, "Upsert " + cloudDBZoneResult + " records");
                if(type == 1) {
                    Toast.makeText(DetailActivity.this, "报名成功！", Toast.LENGTH_SHORT).show();
                } else if(type == 2){
                    Toast.makeText(DetailActivity.this, "取消成功！", Toast.LENGTH_SHORT).show();
                }
                new CountDownTimer(1000, 500) {
                    public void onTick(long millisUntilFinished) {

                    }

                    public void onFinish() {
                        onBackPressed();
                    }
                }.start();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "zzz, failed add," + e.toString());
            }
        });
    }


}