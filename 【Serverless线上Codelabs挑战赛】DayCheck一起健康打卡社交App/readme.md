​
# 【Serverless线上Codelabs挑战赛】】 DayCheck一起健康打卡
## 一、简介

       DayCheck一起健康打卡项目从【Codelabs挑战赛—认证查询】每日健康打卡DayCheck开始，实现了零基础搭建认证查询系统，在学习Serverless认证服务和云数据库服务相关能力后，开发了相关管理打卡的功能。然后，在学习了【集成赢好礼】Codelabs挑战赛——定制简单实用“告白消息”后，使用远程配置、应用内消息和结合云托管实现消息图片的上传，配置消息跳转链接时也可以使用Applinking跳转能力。现在，结合[【集成赢好礼】Codelabs挑战赛-秀出你的Serverless创意能力](https://developer.huawei.com/consumer/cn/forum/topic/0201103926677556230?fid=23)使得该应用实现了原本做好疫情防控工作，并且需要每日健康打卡，来确保动态防控管理之后，又添加了消息通知，远程控制应用界面反馈等实用的功能。

![](https://communityfile-drcn.op.hicloud.com/FileServer/getFile/cmtybbs/227/622/407/0260086000227622407.20221129111618.01727332920047042255106872378065:20221129114904:2800:33CD078DBB32409AE857FF8F1D926EEF7D8261A2CC7372095110E3F8FAEBF94F.gif)

## 二、功能（前面3个功能在之前的文章中已经详细介绍过，这里不在赘述）

### 1、Serverless认证服务

使用AGC认证服务通过申请验证码的方式进行帐号注册和登录，以及退出登录

### 2、Serverless云数据库

AGC云数据库服务提供的端云数据同步的能力，免除了服务器部署、数据管理等工作量，直接客户端开发即可完成

### 3、SmartTable

本应用的数据的展示使用了SmartTable

### 4、远程配置

在HUAWEI AppGallery Connect控制台完成school配置项的设置，实现了远程控制应用的图片、文字显示

![](https://communityfile-drcn.op.hicloud.com/FileServer/getFile/cmtybbs/227/622/407/0260086000227622407.20221129111726.26036408157942696520803365195041:20221129114904:2800:D8FA9A38B8DDEBBC9641FA92E9979843688975BB8ADF1A33F74A723E88685056.png)

核心代码：

    private void initConfig()
    {
        AGConnectConfig config = AGConnectConfig.getInstance();
        long fetchInterval = 0;//12 * 60 * 60L;
        config.fetch(fetchInterval).addOnSuccessListener(configValues -> {
            // Make the configuration parameters take effect
            config.apply(configValues);

            //以String方式获取参数值
            String value = config.getValueAsString("school");
            //showAllValues(config);
            if(value.equals("go"))
            {
                binding.scrollView.stop.setVisibility(View.GONE);
            }
            else
            {
                binding.scrollView.stop.setVisibility(View.VISIBLE);
                binding.scrollView.tips.setText("\n最新通知:\r\n根据教育局最新通知，进行停课安排！");
            }

        }).addOnFailureListener(e -> {
            Toast.makeText(getBaseContext(), "Fetch Fail", Toast.LENGTH_LONG).show();
        });
    }

### 5、应用内消息

在HUAWEI AppGallery Connect控制台添加了应用内消息


核心代码：

    private void initMessage()
    {
        AGConnectAppMessaging appMessaging = AGConnectAppMessaging.getInstance();
        ImageView mb = binding.scrollView.mb;
        mb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appMessaging.setForceFetch();
            }
        });
        appMessaging.addOnDisplayListener(new AGConnectAppMessagingOnDisplayListener() {
            @Override
            public void onMessageDisplay(@NonNull AppMessage appMessage) {
                //远程配置
                Log.i("MessageDisplay","display message success");
                Toast.makeText(DatabaseActivity.this,"Message showed",Toast.LENGTH_LONG).show();
            }
        });
        appMessaging.addOnClickListener(new AGConnectAppMessagingOnClickListener(){
            @Override
            public void onMessageClick(@NonNull AppMessage appMessage, @NonNull Action action) {
                //点击后打开弹框消息设置的url
                String urlStr = action.getActionUrl();
                Log.i(TAG, "getActionUrl: card url"+urlStr);
                Uri url = Uri.parse(urlStr);
                Log.i(TAG, "onMessageClick: message clicked"+url);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(url);
                startActivity(intent);
            }
        });
    }

### 6、Applinking

创建应用内消息时结合云托管实现消息图片的上传，配置消息跳转链接时使用Applinking跳转

## 三、问题

本次学习中遇到了一些问题，在阅读官方文档后得以解决，这里做个提醒：

App Messaging SDK会记录您调用setForceFetch方法的次数，请确保最多5次尝试成功绑定设备的AAID（将您在AGC控制台上配置的测试设备的AAID与当前设备上的AAID保持一致）。如果超过最大尝试次数，则需要卸载应用重装或者清除应用数据，setForceFetch方法的调用次数将会重置。
从远程配置服务使用AGConnectConfig.fetch接口提取远程配置，并调用AGConnectConfig.apply接口传入云端同步获取的值使配置更新，fetchInterval值为0时，能实时获取，不然会以fetchInterval的值为间隔，要时间过后才能再次获取。

## 四、小结

本应用越来越符合学校、单位、公司做好疫情防控工作的功能要求，而且非常简便快捷就能实现，的确值得继续深入学习研究。后续加强社交功能，养成健康打卡的好习惯。
​