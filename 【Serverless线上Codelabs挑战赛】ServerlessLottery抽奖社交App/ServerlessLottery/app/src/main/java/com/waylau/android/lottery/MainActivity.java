package com.waylau.android.lottery;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.huawei.agconnect.auth.AGConnectAuth;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button login = findViewById(R.id.loginbtn);
        TextView id = findViewById(R.id.userid);
        login.setOnClickListener(view -> AGConnectAuth.getInstance().signInAnonymously().addOnSuccessListener(signInResult -> {
            String userId = signInResult.getUser().getUid();
            id.setText(userId);
        }));
		
        Button draw = findViewById(R.id.draw);
        draw.setOnClickListener(view -> {
            Intent intent = new Intent();
            intent.setClass(this, WebActivity.class);
            startActivity(intent);
        });
    }
}