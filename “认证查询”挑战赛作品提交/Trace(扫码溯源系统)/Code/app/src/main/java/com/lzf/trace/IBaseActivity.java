package com.lzf.trace;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.huawei.agconnect.cloud.database.CloudDBZone;
import com.lzf.trace.publics.engine.Engine;
import com.lzf.trace.publics.utils.EmptyUtils;


public abstract class IBaseActivity extends AppCompatActivity {

    protected Context mContext;
    protected IBaseApplication mApplication;

    protected Engine mEngine;

    //    protected KProgressHUD mKProgressHUD;

    /**
     * onCreate
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 设定始终保持竖屏
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mContext = this;

//        FrescoConfigConstants.init(getResources());// 初始化默认图片（占位图，错误图）
//        Fresco.initialize(mContext);// 图片缓存初始化配置

        mApplication = IBaseApplication.getInstance();
        mApplication.mScreenManager.pushActivity(this);// 管理Activity

        mEngine = mApplication.getInstance().getEngine();

    }

    /**
     * 初始化控件
     */
    public abstract void initViews();

    /**
     * 初始化数据
     */
    public abstract void initDatas();

    /**
     * 根据控件id查找控件
     *
     * @param id
     * @return
     */
    @SuppressWarnings("unchecked")
    public <T extends View> T getViewById(int id) {
        return (T) this.findViewById(id);
    }

    /**
     * 设置title
     */
    public void setTitle(String title) {
        TextView titleView = getViewById(R.id.titlename_txt);
        if (EmptyUtils.isNotEmpty(title)) {
            titleView.setText(title);
        }
    }

    /**
     * 返回
     *
     * @param v
     */
    public void onBackPressed(View v) {
        onBackPressed();
        // activity切换动画
    }

    /**
     * 返回
     */
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * onDestroy
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * 滚动条开始
     */
    public void showProgress() {
        // 滚动条
//        mKProgressHUD = KProgressHUD.create(mContext)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("请等待...")
//                .setCancellable(true)
//                .setAnimationSpeed(2)
//                .setDimAmount(0.6f)
//                .show();
    }

    /**
     * 滚动条关闭
     */
    public void dismissProgress() {
//        if (mKProgressHUD != null) {
//            mKProgressHUD.dismiss();
//            mKProgressHUD = null;
//        }
    }
}