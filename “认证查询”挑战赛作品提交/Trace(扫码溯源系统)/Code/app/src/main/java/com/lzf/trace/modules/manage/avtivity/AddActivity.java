package com.lzf.trace.modules.manage.avtivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.huawei.agconnect.AGCRoutePolicy;
import com.huawei.agconnect.AGConnectInstance;
import com.huawei.agconnect.AGConnectOptionsBuilder;
import com.huawei.agconnect.api.AGConnectApi;
import com.huawei.agconnect.auth.AGConnectAuth;
import com.huawei.agconnect.auth.AGConnectAuthCredential;
import com.huawei.agconnect.auth.AGConnectUser;
import com.huawei.agconnect.auth.PhoneAuthProvider;
import com.huawei.agconnect.auth.SignInResult;
import com.huawei.agconnect.auth.VerifyCodeResult;
import com.huawei.agconnect.auth.VerifyCodeSettings;
import com.huawei.agconnect.cloud.database.AGConnectCloudDB;
import com.huawei.agconnect.cloud.database.CloudDBZone;
import com.huawei.agconnect.cloud.database.CloudDBZoneConfig;
import com.huawei.agconnect.cloud.database.exceptions.AGConnectCloudDBException;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;
import com.huawei.hmf.tasks.Task;
import com.huawei.hmf.tasks.TaskExecutors;
import com.king.zxing.CameraScan;
import com.king.zxing.CaptureActivity;
import com.lzf.trace.IBaseActivity;
import com.lzf.trace.R;
import com.lzf.trace.modules.manage.dto.AddEvent;
import com.lzf.trace.modules.manage.dto.ObjectTypeInfoHelper;
import com.lzf.trace.modules.manage.dto.Product;
import com.lzf.trace.modules.scan.avtivity.ResultActivity;
import com.lzf.trace.publics.utils.EmptyUtils;
import com.lzf.trace.publics.utils.L;
import com.lzf.trace.publics.utils.T;
import com.qmuiteam.qmui.widget.QMUITopBar;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;

public class AddActivity extends IBaseActivity {

    private static String countryCodeStr = "86";

    EditText name_et, factory_et, model_et, batch_et, code_et;
    TextView scan_tv, createtime_et, endtime_et;
    Button save_btn;
    private QMUITopBar mTopbar;

    private Product mProduct;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        initViews();
        initDatas();
    }


    @Override
    public void initViews() {
        mTopbar = findViewById(R.id.topbar);
        name_et = findViewById(R.id.name_et);
        factory_et = findViewById(R.id.factory_et);
        model_et = findViewById(R.id.model_et);
        batch_et = findViewById(R.id.batch_et);
        createtime_et = findViewById(R.id.createtime_et);
        endtime_et = findViewById(R.id.endtime_et);
        code_et = findViewById(R.id.code_et);
        save_btn = findViewById(R.id.save_btn);
        scan_tv = findViewById(R.id.scan_tv);

        mTopbar.addLeftBackImageButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        scan_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(mContext, CaptureActivity.class), 111);
            }
        });

        createtime_et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar ca = Calendar.getInstance();
                int mYear = ca.get(Calendar.YEAR);
                int mMonth = ca.get(Calendar.MONTH);
                int mDay = ca.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(mContext,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                Date date = DateUtil.parse("" + year + "/" + (month + 1) + "/" + dayOfMonth);
                                String format = DateUtil.format(date, "yyyy-MM-dd");
                                createtime_et.setText(format);
                            }
                        },
                        mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        endtime_et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar ca = Calendar.getInstance();
                int mYear = ca.get(Calendar.YEAR);
                int mMonth = ca.get(Calendar.MONTH);
                int mDay = ca.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(mContext,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                Date date = DateUtil.parse("" + year + "/" + (month + 1) + "/" + dayOfMonth);
                                String format = DateUtil.format(date, "yyyy-MM-dd");
                                endtime_et.setText(format);
                            }
                        },
                        mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        Intent intent = getIntent();
        String intentData = intent.getStringExtra("product_item");
        Gson mGson = new Gson();
        Product product = mGson.fromJson(intentData, Product.class);
        if (product == null) {
            //新增模式
            mTopbar.setTitle("新增溯源信息");
            mProduct = new Product();
        } else {
            //编辑模式
            mTopbar.setTitle("编辑溯源信息");
            mProduct = product;
            //初始化赋值
            name_et.setText(mProduct.getName());
            factory_et.setText(mProduct.getFactory());
            model_et.setText(mProduct.getModel());
            batch_et.setText(mProduct.getBatch());
            createtime_et.setText(mProduct.getCreatetime());
            endtime_et.setText(mProduct.getEndtime());
            code_et.setText(mProduct.getCode());
        }

    }

    @Override
    public void initDatas() {

        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (EmptyUtils.isEmpty(code_et.getText().toString().trim())) {
                    T.showShort(mContext, "溯源码必填");
                    return;
                }
                mProduct.setName(name_et.getText().toString().trim());
                mProduct.setFactory(factory_et.getText().toString().trim());
                mProduct.setModel(model_et.getText().toString().trim());
                mProduct.setBatch(batch_et.getText().toString().trim());
                mProduct.setCode(code_et.getText().toString().trim());
                mProduct.setCreatetime(createtime_et.getText().toString().trim());
                mProduct.setEndtime(endtime_et.getText().toString().trim());
                if (mProduct.getId() == null) {
                    EventBus.getDefault().post(new AddEvent(true, mProduct));
                } else {
                    EventBus.getDefault().post(new AddEvent(false, mProduct));
                }
                finish();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111 && resultCode == RESULT_OK) {
            String result = CameraScan.parseScanResult(data);
            code_et.setText(result);
        }
    }

}