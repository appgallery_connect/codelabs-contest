package com.lzf.trace;

import android.app.Application;
import android.content.Context;
import android.util.Log;


import com.huawei.agconnect.AGCRoutePolicy;
import com.huawei.agconnect.AGConnectInstance;
import com.huawei.agconnect.AGConnectOptionsBuilder;
import com.huawei.agconnect.auth.AGConnectAuth;
import com.huawei.agconnect.cloud.database.AGConnectCloudDB;
import com.huawei.agconnect.cloud.database.CloudDBZone;
import com.huawei.agconnect.cloud.database.CloudDBZoneConfig;
import com.huawei.agconnect.cloud.database.exceptions.AGConnectCloudDBException;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;
import com.huawei.hmf.tasks.Task;
import com.lzf.trace.modules.manage.dto.ObjectTypeInfoHelper;
import com.lzf.trace.publics.agc.CloudDBZoneWrapper;
import com.lzf.trace.publics.common.ServiceConfig;
import com.lzf.trace.publics.engine.Engine;
import com.lzf.trace.publics.utils.L;
import com.lzf.trace.publics.utils.ScreenManager;
import com.lzf.trace.publics.utils.T;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class IBaseApplication extends Application {

    @SuppressWarnings("unused")
    private Context mContext;

    private Engine mEngine;


    /**
     * 单例
     */
    private static IBaseApplication mBaseApplication;

    /**
     * 管理所有的activity
     */
    public ScreenManager mScreenManager;

    private static IBaseApplication ins;

    // 单例的BaseApplication
    public static IBaseApplication getInstance() {
        if (mBaseApplication == null) {
            mBaseApplication = new IBaseApplication();
        }
        return mBaseApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mBaseApplication = this;
        ins = this;
        mContext = this;
        // 管理所有activity
        mScreenManager = ScreenManager.getScreenManager();

        // 全局异常捕获类
        // CrashHandler crashHandler = CrashHandler.getInstance();
        // crashHandler.init(this);

        mEngine = new Retrofit.Builder()
                .baseUrl("http://" + ServiceConfig.getServicrIp(mContext) + "/p_jnzdgc/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(getOkHttpClient())//使用自己创建的OkHttp，拦截日志
                .build().create(Engine.class);

//        AGConnectCloudDB.initialize(this);
    }

    public static IBaseApplication getIns() {
        return ins;
    }


    public Engine getEngine() {
        return mEngine;
    }


    private OkHttpClient getOkHttpClient() {
        //日志显示级别
        HttpLoggingInterceptor.Level level = HttpLoggingInterceptor.Level.BODY;
        //新建log拦截器
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Log.d("lzf", "OkHttp====Message:" + message);
            }
        });
        loggingInterceptor.setLevel(level);
        //定制OkHttp
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient
                .Builder();
        //OkHttp进行添加拦截器loggingInterceptor
        httpClientBuilder.addInterceptor(loggingInterceptor);
        return httpClientBuilder.build();
    }



}