package com.lzf.trace.publics.common;

import android.content.Context;

import com.lzf.trace.publics.utils.EmptyUtils;
import com.lzf.trace.publics.utils.SharePreferenceUtils;


/**
 * Title: UserInfo<br/>
 * Description: 从SharedPreferences获取用户信息<br/>
 * <p/>
 * Copyright: Copyright (c) 众远科技<br/>
 * Company: 济南众远信息科技有限公司<br/>
 *
 * @author create 高越 2015年11月2日<br/>
 * @version 1.0
 * @since 1.0
 */
public class UserInfo {

    private static SharePreferenceUtils mSharePreferenceUtils;

    private static void initSharePerference(Context context) {
        if (mSharePreferenceUtils == null) {
            mSharePreferenceUtils = new SharePreferenceUtils(context,
                    Constants.USER_INFO_SHARE);
        }
    }

    /**
     * 判断用户是否登录
     *
     * @param context
     * @return
     */
    public static boolean isLogin(Context context) {
        initSharePerference(context);
        String phoneNum = (String) mSharePreferenceUtils.get(
                Constants.TOKEN, "");
        if (EmptyUtils.isEmpty(phoneNum)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 用户id
     *
     * @param context
     * @return
     */
    public static String setUserId(Context context, String id) {
        initSharePerference(context);
        mSharePreferenceUtils.put(Constants.USER_ID, id);
        return id;
    }

    /**
     * 用户id
     *
     * @param context
     * @return
     */
    public static String getUserId(Context context) {
        initSharePerference(context);
        String id = (String) mSharePreferenceUtils.get(Constants.USER_ID,
                "");
        return id;
    }

    /**
     * 用户名
     *
     * @param context
     * @return
     */
    public static String setUserName(Context context, String userName) {
        initSharePerference(context);
        mSharePreferenceUtils.put(Constants.USER_NAME, userName);
        return userName;
    }

    /**
     * 用户名
     *
     * @param context
     * @return
     */
    public static String getUserName(Context context) {
        initSharePerference(context);
        String userName = (String) mSharePreferenceUtils.get(Constants.USER_NAME,
                "");
        return userName;
    }

    /**
     * 手机号
     *
     * @param context
     * @return
     */
    public static String setUserPhone(Context context, String phone) {
        initSharePerference(context);
        mSharePreferenceUtils.put(Constants.USER_PHONE, phone);
        return phone;
    }

    /**
     * 手机号
     *
     * @param context
     * @return
     */
    public static String getUserPhone(Context context) {
        initSharePerference(context);
        String phone = (String) mSharePreferenceUtils.get(Constants.USER_PHONE,
                "");
        return phone;
    }

    /**
     * 用户头像
     *
     * @param context
     * @return
     */
    public static String setPhotoUrl(Context context, String photoUrl) {
        initSharePerference(context);
        mSharePreferenceUtils.put(Constants.PHOTO_URL, photoUrl);
        return photoUrl;
    }

    /**
     * 用户头像
     *
     * @param context
     * @return
     */
    public static String getPhotoUrl(Context context) {
        initSharePerference(context);
        String photoUrl = (String) mSharePreferenceUtils.get(Constants.PHOTO_URL,
                "");
        return photoUrl;
    }

    /**
     * 内部外用户标识（1：外部用户2：内部用户）
     *
     * @param context
     * @return
     */
    public static String setUserClassify(Context context, String userClassify) {
        initSharePerference(context);
        mSharePreferenceUtils.put(Constants.USER_CLASSIFY, userClassify);
        return userClassify;
    }

    /**
     * 内部外用户标识（1：外部用户2：内部用户）
     *
     * @param context
     * @return
     */
    public static String getUserClassify(Context context) {
        initSharePerference(context);
        String userClassify = (String) mSharePreferenceUtils.get(Constants.USER_CLASSIFY,
                "");
        return userClassify;
    }

    /**
     * 性别
     *
     * @param context
     * @return
     */
    public static String setUserSex(Context context, String sex) {
        initSharePerference(context);
        mSharePreferenceUtils.put(Constants.SEX, sex);
        return sex;
    }

    /**
     * 性别
     *
     * @param context
     * @return
     */
    public static String getUserSex(Context context) {
        initSharePerference(context);
        String sex = (String) mSharePreferenceUtils.get(Constants.SEX,
                "");
        return sex;
    }

    /**
     * 是否设置过用户名（0：未设置；1：已设置）
     *
     * @param context
     * @return
     */
    public static String setIsSetUserName(Context context, String isSetUserName) {
        initSharePerference(context);
        mSharePreferenceUtils.put(Constants.ISSETUSERNAME, isSetUserName);
        return isSetUserName;
    }

    /**
     * 是否设置过用户名（0：未设置；1：已设置）
     *
     * @param context
     * @return
     */
    public static String getIsSetUserName(Context context) {
        initSharePerference(context);
        String isSetUserName = (String) mSharePreferenceUtils.get(Constants.ISSETUSERNAME,
                "");
        return isSetUserName;
    }

    /**
     * Token
     *
     * @param context
     * @return
     */
    public static String setToken(Context context, String token) {
        initSharePerference(context);
        mSharePreferenceUtils.put(Constants.TOKEN, token);
        return token;
    }

    /**
     * Token
     *
     * @param context
     * @return
     */
    public static String getToken(Context context) {
        initSharePerference(context);
        String token = (String) mSharePreferenceUtils.get(Constants.TOKEN,
                "");
        return token;
    }

    /**
     * 邮箱
     *
     * @param context
     * @return
     */
    public static String setEmail(Context context, String emailAddress) {
        initSharePerference(context);
        mSharePreferenceUtils.put(Constants.EMAIL, emailAddress);
        return emailAddress;
    }

    /**
     * 邮箱
     *
     * @param context
     * @return
     */
    public static String getEmail(Context context) {
        initSharePerference(context);
        String emailAddress = (String) mSharePreferenceUtils.get(Constants.EMAIL,
                "");
        return emailAddress;
    }

    /**
     * 用户部门
     *
     * @param context
     * @return
     */
    public static String setDepartmentName(Context context, String departmentName) {
        initSharePerference(context);
        mSharePreferenceUtils.put(Constants.DEPARTMENT, departmentName);
        return departmentName;
    }

    /**
     * 用户部门
     *
     * @param context
     * @return
     */
    public static String getDepartmentName(Context context) {
        initSharePerference(context);
        String departmentName = (String) mSharePreferenceUtils.get(Constants.DEPARTMENT,
                "");
        return departmentName;
    }

    /**
     * 用户职级
     *
     * @param context
     * @return
     */
    public static String setGrade(Context context, String grade) {
        initSharePerference(context);
        mSharePreferenceUtils.put(Constants.GRADE, grade);
        return grade;
    }

    /**
     * 用户职级
     *
     * @param context
     * @return
     */
    public static String getGrade(Context context) {
        initSharePerference(context);
        String grade = (String) mSharePreferenceUtils.get(Constants.GRADE,
                "");
        return grade;
    }

    /**
     * 经度
     *
     * @param context
     * @return
     */
    public static String setJingdu(Context context, String jingdu) {
        initSharePerference(context);
        mSharePreferenceUtils.put(Constants.JINGDU, jingdu);
        return jingdu;
    }

    /**
     * 经度
     *
     * @param context
     * @return
     */
    public static String getJingdu(Context context) {
        initSharePerference(context);
        String jingdu = (String) mSharePreferenceUtils.get(Constants.JINGDU,
                "");
        return jingdu;
    }

    /**
     * 纬度
     *
     * @param context
     * @return
     */
    public static String setWeidu(Context context, String weidu) {
        initSharePerference(context);
        mSharePreferenceUtils.put(Constants.GRADE, weidu);
        return weidu;
    }

    /**
     * 纬度
     *
     * @param context
     * @return
     */
    public static String getWeidu(Context context) {
        initSharePerference(context);
        String weidu = (String) mSharePreferenceUtils.get(Constants.GRADE,
                "");
        return weidu;
    }

    /**
     * 退出程序
     *
     * @param context
     */
    public static void signOut(Context context) {
        initSharePerference(context);
        mSharePreferenceUtils.clear();
    }


}