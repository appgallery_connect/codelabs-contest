
import * as auth from './bin/auth'
import * as db from './bin/database'
import schema_surl from './model/surl.json'
import surl from './model/surl';

async function test() { }
let cloudDBZoneName = 'url'
async function getUid() {
    let user = await auth.getUser()
    if (!user) throw '未登录'
    return user.user.uid
}
async function getUserInfo() {
    let AGConnectUser = await auth.getUser()
    console.log('AGConnectUser', AGConnectUser)
    if (!AGConnectUser) throw '未登录'
    let user = AGConnectUser.user
    return {
        success: true,
        data: {
            name: user.displayName || user.phone.substr(-4),
            avatar: 'https://gw.alipayobjects.com/zos/antfincdn/XAosXuNZyF/BiazfanxmamNRoxxVxka.png',
            userid: user.uid,
            email: 'antdesign@alipay.com',
            signature: '海纳百川，有容乃大',
            title: '交互专家',
            group: '蚂蚁金服－某某某事业群－某某平台部－某某技术部－UED',
            tags: [
                {
                    key: '0',
                    label: '很有想法的',
                },
                {
                    key: '1',
                    label: '专注设计',
                },
                {
                    key: '2',
                    label: '辣~',
                },
                {
                    key: '3',
                    label: '大长腿',
                },
                {
                    key: '4',
                    label: '川妹子',
                },
                {
                    key: '5',
                    label: '海纳百川',
                },
            ],
            notifyCount: 12,
            unreadCount: 11,
            country: 'China',
            access: 'admin',
            geographic: {
                province: {
                    label: '浙江省',
                    key: '330000',
                },
                city: {
                    label: '杭州市',
                    key: '330100',
                },
            },
            address: '西湖区工专路 77 号',
            phone: '0752-268888888',
        },
    }
}
async function open() {
    return db.open(schema_surl, cloudDBZoneName)
}
async function queryByNameOne(name) {
    return db.query(surl, query => {
        query.equalTo('name', name)
        query.limit(1)
    })
}
async function querySelf(params) {
    let uid = await getUid()
    console.log('querySelf', uid, params)
    let data = await db.query(surl, query => {
        query.equalTo('uid', uid)
        if (params.name) query.contains('name', params.name)
        query.limit(50)
    })
    return {
        current: 1,
        data,
        pageSize: 10,
        success: true,
        total: data.length
    }
}
async function queryByName(params) {
    console.log('queryByName', params.name)
    return db.query(surl, query => {
        query.equalTo('name', params.name)
        query.limit(1)
    })
}
async function addUrl(params) {
    let uid = await getUid()
    let o = new surl()
    o.name = params.name
    o.des = params.des
    o.url = params.url
    o.ltime = new Date()
    o.uid = uid
    o.hostname = location.hostname
    console.log('addUrl', params, o)
    return db.exec(o)
}
async function del(params) {
    console.log('db.del', params)
    return db.del(params)
}
async function getcode(params) {
    return auth.requestPhoneVerifyCode(params.mobile)
}
async function outLogin(params) {
    return auth.logout()
}
async function login(params) {
    return auth.signInByPhone(params.mobile, params.captcha)
}
export {
    getUid, getUserInfo,
    querySelf, queryByName, addUrl, del,
    open,
    getcode, login, outLogin
}
