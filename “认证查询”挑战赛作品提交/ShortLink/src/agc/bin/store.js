import agconnect from "@agconnect/api";
import "@agconnect/auth";
import "@agconnect/instance";
import agConnectConfig from '../agConnectConfig.json'

agconnect.auth().setUserInfoPersistence(0);
agconnect.instance().configInstance(agConnectConfig);

export {
    agconnect
    , agConnectConfig
}