package com.haoc.lostandfound.auth;


import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.haoc.lostandfound.R;
import com.haoc.lostandfound.utils.Myutils;
import com.huawei.agconnect.auth.AGConnectAuth;
import com.huawei.agconnect.auth.AGConnectAuthCredential;
import com.huawei.agconnect.auth.AGConnectUser;
import com.huawei.agconnect.auth.EmailAuthProvider;
import com.huawei.agconnect.auth.SignInResult;
import com.huawei.agconnect.auth.VerifyCodeSettings;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;
import com.huawei.hmf.tasks.TaskExecutors;

public class EmailActivity extends Activity {
    private EditText accountEdit;
    private EditText verifyCodeEdit;
    Button send;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_link);
        accountEdit = findViewById(R.id.et_account);
        verifyCodeEdit = findViewById(R.id.et_verify_code);
        send = findViewById(R.id.btn_send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendVerificationCode();
            }
        });

        findViewById(R.id.btn_link).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = accountEdit.getText().toString().trim();
                String verifyCode = verifyCodeEdit.getText().toString().trim();
                /*
                通过邮箱和验证码获取凭证。
                如果verifyCode为空，此函数会调用credentialWithPassword接口。
                Link操作时，verifyCode必填。
                 */
                AGConnectAuthCredential credential = EmailAuthProvider.credentialWithVerifyCode(email, null, verifyCode);
                AGConnectUser agcUser = AGConnectAuth.getInstance().getCurrentUser();
                if (agcUser != null) {
                    // 关联邮箱
                    agcUser.link(credential)
                            .addOnSuccessListener(TaskExecutors.uiThread(), new OnSuccessListener<SignInResult>() {
                                @Override
                                public void onSuccess(SignInResult signInResult) {
                                    Toast.makeText(EmailActivity.this, "关联邮箱成功", Toast.LENGTH_SHORT).show();
                                }
                            }).addOnFailureListener(TaskExecutors.uiThread(), new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {
                            Toast.makeText(EmailActivity.this, "关联邮箱失败:" + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });

        findViewById(R.id.btn_unlink).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AGConnectAuth.getInstance().getCurrentUser() != null) {
                    // 解除邮箱关联
                    AGConnectAuth.getInstance().getCurrentUser().unlink(AGConnectAuthCredential.Email_Provider)
                            .addOnSuccessListener(TaskExecutors.uiThread(), new OnSuccessListener<SignInResult>() {
                                @Override
                                public void onSuccess(SignInResult signInResult) {
                                    Toast.makeText(EmailActivity.this, "解除邮箱关联成功", Toast.LENGTH_SHORT).show();
                                }
                            }).addOnFailureListener(TaskExecutors.uiThread(), new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {
                            Toast.makeText(EmailActivity.this, "解除邮箱关联失败:" + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
    }

    private void sendVerificationCode() {
        String email = accountEdit.getText().toString().trim();
        Myutils.sendVerificationCodeByEmail(EmailActivity.this, send, email, VerifyCodeSettings.ACTION_REGISTER_LOGIN);
    }
}