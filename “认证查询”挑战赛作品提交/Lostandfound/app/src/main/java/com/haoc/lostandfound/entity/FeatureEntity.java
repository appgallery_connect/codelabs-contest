package com.haoc.lostandfound.entity;

import java.io.Serializable;

/**
 * @description: 功能介绍实体类
 * @author: Haoc
 * @Time: 2022/4/29  11:17
 */

public class FeatureEntity implements Serializable {
    private String titleFeature;
    private String Date;
    private String Details;

    public FeatureEntity(String titleFeature, String date, String details) {
        this.titleFeature = titleFeature;
        Date = date;
        Details = details;
    }

    public String getTitleFeature() {
        return titleFeature;
    }

    public void setTitleFeature(String titleFeature) {
        this.titleFeature = titleFeature;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getDetails() {
        return Details;
    }

    public void setDetails(String details) {
        Details = details;
    }
}
