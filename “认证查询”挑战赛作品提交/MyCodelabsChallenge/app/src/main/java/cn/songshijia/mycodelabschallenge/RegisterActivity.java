package cn.songshijia.mycodelabschallenge;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.huawei.agconnect.auth.AGConnectAuth;
import com.huawei.agconnect.auth.PhoneUser;
import com.huawei.agconnect.auth.SignInResult;
import com.huawei.agconnect.auth.VerifyCodeResult;
import com.huawei.agconnect.auth.VerifyCodeSettings;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;
import com.huawei.hmf.tasks.Task;
import com.huawei.hmf.tasks.TaskExecutors;

import java.util.Locale;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        EditText phoneNumber = findViewById(R.id.phone_number);
        EditText verifyCode = findViewById(R.id.verify_code);
        EditText password = findViewById(R.id.password);
        Context context = this;
        findViewById(R.id.query_verify_code).setOnClickListener(v -> {
            phoneNumber.setClickable(false);
            String phone = phoneNumber.getText().toString();
            if (phone.length() > 0) {
                VerifyCodeSettings settings = new VerifyCodeSettings.Builder()
                        .action(VerifyCodeSettings.ACTION_REGISTER_LOGIN)
                        .sendInterval(30)
                        .locale(Locale.CHINA)
                        .build();
                Task<VerifyCodeResult> task = AGConnectAuth.getInstance()
                        .requestVerifyCode("+86", phone, settings);
                task.addOnSuccessListener(TaskExecutors.uiThread(), verifyCodeResult -> {
                    //验证码申请成功
                    System.out.println("验证码申请成功");
                    Toast.makeText(this, "验证码申请成功", Toast.LENGTH_SHORT).show();
                }).addOnFailureListener(TaskExecutors.uiThread(), e -> {
                    e.printStackTrace();
                    Toast.makeText(this, e.getMessage() + "：验证码申请失败", Toast.LENGTH_LONG).show();
                });
            } else {
                Toast.makeText(this, "手机号码为空，验证码申请失败", Toast.LENGTH_LONG).show();
            }
        });

        findViewById(R.id.phone_register).setOnClickListener(v -> {
            String phone = phoneNumber.getText().toString();
            String code = verifyCode.getText().toString();
            String passwd = password.getText().toString();
            if (phone.length() > 0 && code.length() > 0 && passwd.length() > 0) {
                PhoneUser phoneUser = new PhoneUser.Builder()
                        .setCountryCode("+86")
                        .setPhoneNumber(phoneNumber.getText().toString())
                        .setVerifyCode(verifyCode.getText().toString())
                        .setPassword(passwd)  //optional
                        .build();
                AGConnectAuth.getInstance().createUser(phoneUser)
                        .addOnSuccessListener(new OnSuccessListener<SignInResult>() {
                            @Override
                            public void onSuccess(SignInResult signInResult) {
                                //创建帐号成功后，默认已登录
                                System.out.println("创建帐号成功:" + signInResult.getUser().getUid());
                                Toast.makeText(context, "创建帐号成功",
                                        Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent();
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.setClass(context, DataActivity.class);
                                startActivity(intent);
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(Exception e) {
                                e.printStackTrace();
                                Toast.makeText(context, e.getMessage() + "：创建帐号失败",
                                        Toast.LENGTH_LONG).show();
                            }
                        });
            } else {
                Toast.makeText(this, "验证码或密码为空，注册失败", Toast.LENGTH_LONG).show();
            }
        });
    }
}