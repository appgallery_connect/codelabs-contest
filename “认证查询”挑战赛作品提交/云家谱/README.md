### config plugin
#### project build.gradle
```
buildscript {
repositories {
maven { url file('repo').toURI() }
maven { url 'https://developer.huawei.com/repo' }
google()
mavenCentral()
}
dependencies {
classpath "com.android.tools.build:gradle:7.0.4"
classpath "com.huawei.agconnect:agcp:1.6.4.300"
// NOTE: Do not place your application dependencies here; they belong
// in the individual module build.gradle files
}
}
```
#### app build.gradle
```
plugins {
    id 'com.android.application'
    id 'com.huawei.agconnect'
}

```
```
dependencies {

    implementation 'androidx.appcompat:appcompat:1.2.0'
    implementation 'com.google.android.material:material:1.3.0'
    implementation 'androidx.constraintlayout:constraintlayout:2.0.4'
    testImplementation 'junit:junit:4.+'
    androidTestImplementation 'androidx.test.ext:junit:1.1.2'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.3.0'

    implementation 'com.huawei.agconnect:agconnect-core:1.6.5.300'
    implementation "com.huawei.agconnect:agconnect-auth:1.6.4.300"
    implementation 'com.huawei.agconnect:agconnect-cloud-database:1.5.2.300'
    implementation 'com.squareup.picasso:picasso:2.71828'

    // huawei id
    implementation "com.huawei.hms:hwid:5.2.0.300"
}
```
### login auther
#### 第三方 华为登录时 报6003的解决方案
    6003 是指证书指纹签名异常，两个步骤解决：agc项目中填入SHA256后下载agcconnect-serices.json文件，打包运行时在project structure-》module->signing configs中
    选择生成的jks文件，并填入密码，然后在打包生成。
#### 认证服务的用户
    认证服务产生的用户属性有限，使用AGConnectUser agConnectUser = AGConnectAuth.getInstance().getCurrentUser()来管理登录用户。
### cloud database
#### 连接数据库失败
     重新核对下云数据库产生的版本，一般不要改动下载的数据格式文件。然后清理缓存，最好卸载应用程序重新安装。
#### 模拟器闪退
    云数据库插件部分文件不支持x86,需要使用真机来调试。

   