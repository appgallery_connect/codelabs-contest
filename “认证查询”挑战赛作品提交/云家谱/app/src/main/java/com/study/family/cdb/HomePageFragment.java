/*
 * Copyright 2020. Huawei Technologies Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.study.family.cdb;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.huawei.agconnect.auth.AGConnectAuth;
import com.huawei.agconnect.auth.AGConnectUser;
import com.study.family.model.FamilyEditFields;
import com.study.family.model.FamilyInfo;
import com.study.family.model.CloudDBZoneWrapper;
import com.study.family.model.FamilyEditFields;
import com.study.family.model.FamilyInfo;
import com.study.family.utils.DateUtils;
import com.study.family.R;
import com.huawei.agconnect.cloud.database.CloudDBZoneQuery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class HomePageFragment extends Fragment
    implements CloudDBZoneWrapper.UiCallBack {
    // Request code for clicking search menu
    private static final int REQUEST_SEARCH = 1;

    // Request code for clicking add menu
    private static final int REQUEST_ADD = 2;

    // Store current state of sort order
    private final SortState mSortState = new SortState();

    private final QueryInfo mQueryInfo = new QueryInfo();

    private final CloudDBZoneWrapper mCloudDBZoneWrapper;

    private Handler mHandler = null;

    private FamilyInfoAdapter mFamilyInfoAdapter;

    private View mHeaderView;

    private View mBottomPanelView;

    private Menu mOptionMenu;

    private boolean mInSearchMode = false;

    private CdbActivity mActivity;

    public HomePageFragment() {

        mCloudDBZoneWrapper = new CloudDBZoneWrapper();
    }

    static Fragment newInstance() {
        return new HomePageFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mActivity = (CdbActivity) getActivity();
         mHandler = new Handler(Looper.getMainLooper());
        mFamilyInfoAdapter = new FamilyInfoAdapter(getContext());
        mHandler.post(() -> {

              cloudDBinit();
        });
    }

    private void cloudDBinit() {
        mCloudDBZoneWrapper.addCallBacks(HomePageFragment.this);
        mCloudDBZoneWrapper.createObjectType();
        mCloudDBZoneWrapper.openCloudDBZoneV2();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
        @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        initFamilyListHeaderViews(rootView);
        initFamilyListView(rootView);
        initBottomPanelView(rootView);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, MenuInflater inflater) {
        mOptionMenu = menu;
        inflater.inflate(R.menu.book_manager_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        AGConnectUser agConnectUser = AGConnectAuth.getInstance().getCurrentUser();
        String nickname = agConnectUser.getDisplayName();
        String phone = agConnectUser.getPhone();
        if((phone!=null)) {
            Intent intent = new Intent();
            if (getContext() != null) {
                intent.setClass(getContext(), EditActivity.class);
            }
            int id = item.getItemId();
            if (id == R.id.search_button) {
                intent.setAction(EditActivity.ACTION_SEARCH);
                startActivityForResult(intent, REQUEST_SEARCH);
            } else if (id == R.id.add_button) {
                intent.putExtra(FamilyEditFields.EDIT_MODE, FamilyEditFields.EditMode.ADD.name());
                intent.setAction(EditActivity.ACTION_ADD);
                startActivityForResult(intent, REQUEST_ADD);
            } else if (id == R.id.select_all) {
                mFamilyInfoAdapter.onSelectAllMenuClicked();
                updateSelectAllMenuHint(item);
            } else if (id == R.id.show_all) {
                restoreQueryState();
                mHandler.post(mCloudDBZoneWrapper::queryAllFamilys);
                toggleShowAllState(false);
            }
        }
        else {
            Toast.makeText(getActivity(), "游客不能使用，请使用注册账户登录", Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_CANCELED) {
            return;
        }
        restoreQueryState();
        if (requestCode == REQUEST_ADD) {
            processAddAction(data);
        } else {
            processSearchAction(data);
        }
    }

    private void restoreQueryState() {
        mQueryInfo.clean();
        cleanSortState();
    }

    private void processSearchAction(Intent data) {
//        CloudDBZoneQuery<FamilyInfo> query = CloudDBZoneQuery.where(FamilyInfo.class);
//        String Name = data.getStringExtra(FamilyEditFields.FAMILY_NAME);
//        if (!TextUtils.isEmpty(Name)) {
//            query.contains(FamilyEditFields.FAMILY_NAME, Name);
//            mQueryInfo.familyName = Name;
//        }
//
//        double lowestPrice = data.getDoubleExtra(FamilyEditFields.LOWEST_PRICE, 0);
//        double highestPrice = data.getDoubleExtra(FamilyEditFields.HIGHEST_PRICE, 0);
//
//        if (lowestPrice > highestPrice) {
//            double temp = lowestPrice;
//            lowestPrice = highestPrice;
//            highestPrice = temp;
//        }
//        mQueryInfo.lowestPrice = lowestPrice;
//        mQueryInfo.highestPrice = highestPrice;
//
//        query.greaterThanOrEqualTo(FamilyEditFields.PRICE, lowestPrice);
//        if (mQueryInfo.lowestPrice != mQueryInfo.highestPrice || mQueryInfo.lowestPrice != 0.0) {
//            query.lessThanOrEqualTo(FamilyEditFields.PRICE, mQueryInfo.highestPrice);
//        }
//
//        int showCount = data.getIntExtra(FamilyEditFields.SHOW_COUNT, 0);
//        if (showCount > 0) {
//            query.limit(showCount);
//            mQueryInfo.showCount = showCount;
//        }
//
//        toggleShowAllState(true);
//        mHandler.post(() -> mCloudDBZoneWrapper.queryFamilys(query));
    }

    private void processAddAction(Intent data) {
        FamilyInfo familyInfo = new FamilyInfo();
        int familyID = data.getIntExtra(FamilyEditFields.FAMILY_ID, -1);
        if (familyID == -1) {
            familyInfo.setID(mCloudDBZoneWrapper.getFamilyIndex() + 1);
        } else {
            familyInfo.setID(familyID);
        }
        familyInfo.setName(data.getStringExtra(FamilyEditFields.FAMILY_NAME));
        familyInfo.setSex(data.getStringExtra(FamilyEditFields.SEX));
        familyInfo.setBirthday(data.getStringExtra(FamilyEditFields.BIRTHDAY));
        familyInfo.setDeathday(data.getStringExtra(FamilyEditFields.DEATHDAY));
        familyInfo.setInstruction(data.getStringExtra(FamilyEditFields.INSTRUCTION));
        toggleShowAllState(false);
        mHandler.post(() -> mCloudDBZoneWrapper.upsertFamilyInfos(familyInfo));
    }

    @Override
    public void onDestroy() {
        mHandler.post(mCloudDBZoneWrapper::closeCloudDBZone);
        super.onDestroy();
    }

    private void initFamilyListHeaderViews(View rootView) {
        // mHeaderView = inflater.inflate(R.layout.family_manager_header, null);
        mHeaderView = rootView.findViewById(R.id.table_header);

        View familyNameHeaderView = mHeaderView.findViewById(R.id.header_book_name);
        TextView familyNameTitleView = familyNameHeaderView.findViewById(R.id.title_name);
        familyNameTitleView.setText(R.string.family_name);
        familyNameTitleView.setOnClickListener(v -> {
            updateSortState(FamilyEditFields.FAMILY_NAME, familyNameHeaderView);
            queryWithOrder();
        });

        View authorHeaderView = mHeaderView.findViewById(R.id.header_author);
        TextView authorTitleView = authorHeaderView.findViewById(R.id.title_name);
        authorTitleView.setText(R.string.sex);
        authorHeaderView.setOnClickListener(v -> {
            updateSortState(FamilyEditFields.SEX, authorHeaderView);
            queryWithOrder();
        });

        View priceHeaderView = mHeaderView.findViewById(R.id.header_price);
        TextView priceTitleView = priceHeaderView.findViewById(R.id.title_name);
        priceTitleView.setText(R.string.birthday);
        priceHeaderView.setOnClickListener(v -> {
            updateSortState(FamilyEditFields.BIRTHDAY, priceHeaderView);
            queryWithOrder();
        });

        View borrowerHeaderView = mHeaderView.findViewById(R.id.header_publisher);
        TextView publisherTitleView = borrowerHeaderView.findViewById(R.id.title_name);
        publisherTitleView.setText(R.string.deathday);
        borrowerHeaderView.setOnClickListener(v -> {
            updateSortState(FamilyEditFields.DEATHDAY, borrowerHeaderView);
            queryWithOrder();
        });

        View publishTimeHeaderView = mHeaderView.findViewById(R.id.header_publish_time);
        TextView publishTimeTitleView = publishTimeHeaderView.findViewById(R.id.title_name);
        publishTimeTitleView.setText(R.string.instruction);
        publishTimeHeaderView.setOnClickListener(v -> {
            updateSortState(FamilyEditFields.INSTRUCTION, publishTimeHeaderView);
            queryWithOrder();
        });
    }

    private void initFamilyListView(View rootView) {
        ListView familyListView = rootView.findViewById(R.id.book_list);
        familyListView.setAdapter(mFamilyInfoAdapter);
        familyListView.setOnItemClickListener((parent, view, position, id) -> {
            touchItem(position);
        });
        familyListView.setOnItemLongClickListener((parent, view, position, id) -> {
            touchLongItem(position);
            return false;
        });
    }

    private void touchLongItem(int position) {
        AGConnectUser agConnectUser = AGConnectAuth.getInstance().getCurrentUser();
        String nickname = agConnectUser.getDisplayName();
        String phone = agConnectUser.getPhone();
        if((phone!=null)) {
            if (!mFamilyInfoAdapter.isMultipleMode()) {
                mFamilyInfoAdapter.setMultipleMode(true);
                invalidateViewInMultipleChoiceMode();
            }
        }
        else
        {
            Toast.makeText(getActivity(), "游客不能使用，请使用注册账户登录", Toast.LENGTH_SHORT).show();
        }
    }

    private void touchItem(int position) {
        AGConnectUser agConnectUser = AGConnectAuth.getInstance().getCurrentUser();
        String nickname = agConnectUser.getDisplayName();
        String phone = agConnectUser.getPhone();
        if((phone!=null)) {
            if (mFamilyInfoAdapter.isMultipleMode()) {
                mFamilyInfoAdapter.check(position);
                updateSelectAllMenuHint(mOptionMenu.findItem(R.id.select_all));
            } else {
                FamilyInfo familyInfo = (FamilyInfo) mFamilyInfoAdapter.getItem(position);
                Intent intent = new Intent();
                if (getContext() != null) {
                    intent.setClass(getContext(), EditActivity.class);
                }
                intent.putExtra(FamilyEditFields.FAMILY_ID, familyInfo.getID());
                intent.putExtra(FamilyEditFields.FAMILY_NAME, familyInfo.getName());
                intent.putExtra(FamilyEditFields.SEX, familyInfo.getSex());
                intent.putExtra(FamilyEditFields.BIRTHDAY, familyInfo.getBirthday());
                intent.putExtra(FamilyEditFields.DEATHDAY, familyInfo.getDeathday());
                intent.putExtra(FamilyEditFields.FATHERID, familyInfo.getFatherID());
                intent.putExtra(FamilyEditFields.IMAURL, familyInfo.getImgurl());
                intent.putExtra(FamilyEditFields.INSTRUCTION, familyInfo.getInstruction());
                
                intent.putExtra(FamilyEditFields.EDIT_MODE, FamilyEditFields.EditMode.MODIFY.name());
                intent.setAction(EditActivity.ACTION_ADD);
                startActivityForResult(intent, REQUEST_ADD);
            }
        }
        else
        {
            Toast.makeText(getActivity(), "游客不能使用，请使用注册账户登录", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (mFamilyInfoAdapter.isMultipleMode()) {
            invalidateViewInMultipleChoiceMode();
        } else {
            invalidateViewInNormalMode();
        }
    }

    private void initBottomPanelView(View rootView) {
        mBottomPanelView = rootView.findViewById(R.id.buttonPanel);
        Button deleteButton = mBottomPanelView.findViewById(R.id.delete);
        deleteButton.setOnClickListener(v -> {
            List<FamilyInfo> familyInfoList = mFamilyInfoAdapter.getSelectedItems();
            mHandler.post(() -> mCloudDBZoneWrapper.deleteFamilyInfos(familyInfoList));
            invalidateViewInNormalMode();
        });
        Button cancelButton = mBottomPanelView.findViewById(R.id.cancel);
        cancelButton.setOnClickListener(v -> invalidateViewInNormalMode());
    }

    private void invalidateViewInMultipleChoiceMode() {
        mHeaderView.findViewById(R.id.checkbox).setVisibility(View.INVISIBLE);
        mBottomPanelView.setVisibility(View.VISIBLE);
        mActivity.hideNavigationBar();

        int menuSize = mOptionMenu.size();
        for (int i = 0; i < menuSize; i++) {
            MenuItem menuItem = mOptionMenu.getItem(i);
            if (menuItem.getItemId() == R.id.select_all) {
                menuItem.setVisible(true);
                menuItem.setTitle(R.string.select_all);
            } else {
                menuItem.setVisible(false);
            }
        }
    }

    private void invalidateViewInNormalMode() {
        mHeaderView.findViewById(R.id.checkbox).setVisibility(View.GONE);
        mBottomPanelView.setVisibility(View.GONE);
        mActivity.showNavigationBar();
        mFamilyInfoAdapter.setMultipleMode(false);

        int menuSize = mOptionMenu.size();
        for (int i = 0; i < menuSize; i++) {
            MenuItem menuItem = mOptionMenu.getItem(i);
            int id = menuItem.getItemId();
            if (id == R.id.select_all) {
                menuItem.setVisible(false);
            } else if (id == R.id.add_button) {
                menuItem.setVisible(!mInSearchMode);
            } else if (id == R.id.show_all) {
                menuItem.setVisible(mInSearchMode);
            } else {
                menuItem.setVisible(true);
            }
        }
    }

    private void updateSelectAllMenuHint(MenuItem menuItem) {
        if (mFamilyInfoAdapter.isAllSelected()) {
            menuItem.setTitle(R.string.select_none);
        } else {
            menuItem.setTitle(R.string.select_all);
        }
    }

    private void toggleShowAllState(boolean inSearchMode) {
        mInSearchMode = inSearchMode;
        invalidateViewInNormalMode();
        mOptionMenu.findItem(R.id.show_all).setVisible(mInSearchMode);
    }

    @Override
    public void onAddOrQuery(List<FamilyInfo> familyInfoList) {
        mHandler.post(() -> {
            // Object is sorted by default encoding, so we need to resort it by pinyin for Chinese
            if (mSortState.state != null && (FamilyEditFields.FAMILY_NAME.equals(mSortState.field)
                || FamilyEditFields.SEX.equals(mSortState.field) || FamilyEditFields.INSTRUCTION.equals(
                mSortState.field))) {
                Collections.sort(familyInfoList, (o1, o2) -> {
                    int result;
                    if (FamilyEditFields.FAMILY_NAME.equals(mSortState.field)) {
                        result = o1.getName().compareTo(o2.getName());
                    } else if (FamilyEditFields.SEX.equals(mSortState.field)) {
                        result = o1.getSex().compareTo(o2.getSex());
                    } else {
                        result = o1.getInstruction().compareTo(o2.getInstruction());
                    }
                    if (mSortState.state == SortState.State.UP) {
                        return result;
                    } else {
                        return -result;
                    }
                });
            }
            mFamilyInfoAdapter.addFamilys(familyInfoList);
        });
    }

    @Override
    public void onSubscribe(List<FamilyInfo> familyInfoList) {
        if (!mInSearchMode) {
            mHandler.post(() -> {
                if (familyInfoList.isEmpty()) {
                    Toast.makeText(getContext(), R.string.empty_family_list, Toast.LENGTH_SHORT).show();
                }
                mFamilyInfoAdapter.addFamilys(familyInfoList);
            });
        }
    }

    @Override
    public void onDelete(List<FamilyInfo> familyInfoList) {
        mHandler.post(() -> mFamilyInfoAdapter.deleteFamilys(familyInfoList));
    }

    @Override
    public void updateUiOnError(String errorMessage) {
        // dummy
    }

    private void updateSortState(String field, View view) {
        cleanSortState();

        mSortState.field = field;
        ImageView newDownArrowView = view.findViewById(R.id.arrow_down);
        ImageView newUpArrowView = view.findViewById(R.id.arrow_up);
        if (mSortState.state == SortState.State.UP) {
            mSortState.state = SortState.State.DOWN;
            newDownArrowView.setImageResource(R.drawable.arrow_down_selected);
        } else {
            newUpArrowView.setImageResource(R.drawable.arrow_up_selected);
            mSortState.state = SortState.State.UP;
        }
        mSortState.downArrowView = newDownArrowView;
        mSortState.upArrowView = newUpArrowView;
    }

    private void cleanSortState() {
        mSortState.field = null;
        if (mSortState.upArrowView != null) {
            mSortState.upArrowView.setImageResource(R.drawable.arrow_up);
        }
        if (mSortState.downArrowView != null) {
            mSortState.downArrowView.setImageResource(R.drawable.arrow_down);
        }
    }

    private void queryWithOrder() {
        invalidateViewInNormalMode();
        CloudDBZoneQuery<FamilyInfo> query = CloudDBZoneQuery.where(FamilyInfo.class);
        if (!mQueryInfo.familyName.isEmpty()) {
            query.contains(FamilyEditFields.FAMILY_NAME, mQueryInfo.familyName);
        }
         if (mQueryInfo.showCount > 0) {
            query.limit(mQueryInfo.showCount);
        }
        if (mSortState.state == SortState.State.UP) {
            query.orderByAsc(mSortState.field);
        } else {
            query.orderByDesc(mSortState.field);
        }
        mHandler.post(() -> mCloudDBZoneWrapper.queryFamilys(query));
    }

    private static final class SortState {
        String field;

        ImageView upArrowView;

        ImageView downArrowView;

        State state;

        enum State {
            UP,
            DOWN
        }
    }

    private static class FamilyInfoAdapter extends BaseAdapter {
        private final List<FamilyInfo> mFamilyList = new ArrayList<>();

        private final SparseBooleanArray mCheckState = new SparseBooleanArray();

        private final LayoutInflater mInflater;

        private boolean mIsMultipleMode = false;

        FamilyInfoAdapter(Context context) {
            mInflater = LayoutInflater.from(context);
        }

        void addFamilys(List<FamilyInfo> familyInfoList) {
            mFamilyList.clear();
            mFamilyList.addAll(familyInfoList);
            notifyDataSetChanged();
        }

        void deleteFamilys(List<FamilyInfo> FamilyInfoList) {
            for (FamilyInfo familyInfo : FamilyInfoList) {
                mFamilyList.remove(familyInfo);
            }
            notifyDataSetChanged();
        }

        void check(int position) {
            if (mCheckState.get(position)) {
                mCheckState.delete(position);
            } else {
                mCheckState.put(position, true);
            }
            notifyDataSetChanged();
        }

        void onSelectAllMenuClicked() {
            // Current state is all elements selected, so switch the state to none selected
            if (mCheckState.size() == getCount()) {
                mCheckState.clear();
            } else {
                mCheckState.clear();
                for (int i = 0; i < getCount(); i++) {
                    mCheckState.put(i, true);
                }
            }
            notifyDataSetChanged();
        }

        boolean isAllSelected() {
            return mCheckState.size() == mFamilyList.size();
        }

        List<FamilyInfo> getSelectedItems() {
            List<FamilyInfo> familyInfoList = new ArrayList<>();
            int selectCount = mCheckState.size();
            for (int i = 0; i < selectCount; i++) {
                familyInfoList.add(mFamilyList.get(mCheckState.keyAt(i)));
            }
            return familyInfoList;
        }

        boolean isMultipleMode() {
            return mIsMultipleMode;
        }

        void setMultipleMode(boolean isMultipleMode) {
            mIsMultipleMode = isMultipleMode;
            if (!isMultipleMode) {
                mCheckState.clear();
            }
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mFamilyList.size();
        }

        @Override
        public Object getItem(int position) {
            return mFamilyList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = mInflater.inflate(R.layout.book_list_item, null);
                viewHolder.authorView = convertView.findViewById(R.id.author);
                viewHolder.familyNameView = convertView.findViewById(R.id.book_name);
                viewHolder.priceView = convertView.findViewById(R.id.price);
                viewHolder.publisherView = convertView.findViewById(R.id.publisher);
                viewHolder.publishTimeView = convertView.findViewById(R.id.publish_time);
                viewHolder.checkBox = convertView.findViewById(R.id.checkbox);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            FamilyInfo familyInfo = mFamilyList.get(position);
            viewHolder.authorView.setText(familyInfo.getSex());
            viewHolder.familyNameView.setText(familyInfo.getName());
            viewHolder.priceView.setText(familyInfo.getBirthday());
            viewHolder.publisherView.setText(familyInfo.getDeathday());
            viewHolder.publishTimeView.setText(familyInfo.getFatherID());
            if (mIsMultipleMode) {
                viewHolder.checkBox.setVisibility(View.VISIBLE);
            } else {
                viewHolder.checkBox.setVisibility(View.GONE);
            }
            viewHolder.checkBox.setChecked(mCheckState.get(position, false));
            return convertView;
        }
    }

    private static final class ViewHolder {
        CheckBox checkBox;

        TextView familyNameView;

        TextView authorView;

        TextView priceView;

        TextView publisherView;

        TextView publishTimeView;
    }

    private static final class QueryInfo {
        String familyName = "";

        double lowestPrice = 0.0;

        double highestPrice = 0.0;

        int showCount = 0;

        void clean() {
            familyName = "";
            lowestPrice = 0.0;
            highestPrice = 0.0;
            showCount = 0;
        }
    }
}
